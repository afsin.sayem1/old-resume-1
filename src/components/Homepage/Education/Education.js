import React, { useEffect, useState } from 'react';

const Education = ({education}) => {
    const [edu, setEdu] = useState({});

    useEffect( () => {
        education[0] && setEdu(education[0])
    },[education])

    return (
        <div id="education">
            <h2 className="educ-title"> Education </h2>
            
            <ul className="educ-choose">
                {
                    education.map((educValue, index) => {
                        return (
                            <li 
                                key={index}
                                
                                className={`educ-choose-item ${educValue.studyType == edu.studyType ? "educ-choose-choosed" : ""}`}
                            >   
                                <button
                                    className="educ-chose-item-button"
                                    onClick={ (e) => {
                                        e.stopPropagation();
                                        setEdu(education[index]);
                                    }} 
                                >
                                    {educValue.studyType.toLowerCase()}
                                </button>
                            </li>
                        );
                    })
                }
            </ul>

            <div className="educ-info">
                {edu.institutionFirstPart && <span> Institution : {edu.institutionFirstPart + " " + edu.institutionSecondPart} </span>}
                {edu.area && <span> Area : {edu.area} </span>}
                {edu.endDate && <span> End Date : {edu.endDate} </span>}
            </div>
        </div>
    );
};

export default Education;