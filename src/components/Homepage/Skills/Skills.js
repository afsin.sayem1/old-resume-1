import React from 'react';

const Skills = ({ skills }) => {
	return (
		<div id="skills" >
			<h1 className="skills-title"> Skills </h1>
			<div className="skills-info">
				{
					skills.map((skill,index) => {
						return (
							<div 
								key={index}
								className="skill"
							>
								<h3 className="skill-subtitle"> {skill.name} </h3>
								<p className="skill-desc"> 
									{
										skill.keywords.map((keyword, index) => { 
											const word = index !== (skill.keywords.length - 1) ? `${keyword}, ` : keyword;
											return word;
										}
									)} 
								</p>
							</div>
						)
					})
				}

			</div>

		</div>
	);
};

export default Skills;