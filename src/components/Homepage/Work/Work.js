import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faPause } from '@fortawesome/free-solid-svg-icons';


const Work = ({ work, project, achievement, downloadLinks, handlePrevClick, handleNextClick, curMode }) => {
	const [audio, setAudio] = useState();
	const [active, setActive] = useState(false);

	useEffect(() => {
		if (audio) audio.pause()

		if (work.audioName) {
			setAudio(new Audio(`./data/audio/${work.audioName}`));
			setActive(false);
		} else {
			setAudio(false)
		}
	}, [work]);

	useEffect(() => {
		if (curMode != "experience" && audio) audio.pause()
	}, [curMode, audio])

	return (
		<div id="work">
			<button
				className="work-left-btn"
				aria-label="previous"
				onClick={handlePrevClick}
			/>
			<button
				className="work-right-btn"
				aria-label="next"
				onClick={handleNextClick}
			/>
			{
			  downloadLinks &&
				<div className="work-button-group">
					<a href={downloadLinks[0].link} download className="work-link"><span>{downloadLinks[0].name}</span></a>
					<a href={downloadLinks[1].link} download className="work-link"><span>{downloadLinks[1].name}</span></a>
					<span className="work-splitter-cont">
						<span className="work-splitter">or</span>
					</span>
				</div>
			}

			{
				curMode == "experience" ?
				<div className="work-info">
					{
						audio &&
						<div className="btn-play-pause">
							{!active ?
								<FontAwesomeIcon onClick={(e) => {
									e.stopPropagation();
									audio.play();
									setActive(!active);
									audio.addEventListener('ended', () => setActive(false));
								}} icon={faPlay} /> :
								<FontAwesomeIcon onClick={(e) => {
									e.stopPropagation();
									audio.pause();
									setActive(!active);
								}} icon={faPause} />}
						</div>
					}

					<div className="work-info-text" >
						<span className="company"> <span> $ </span> {work?.company} </span>
						<span className="position" style={{ color: "#2CDA00" }}> <span> $ </span> {work?.end?.position} </span>
						{work?.start?.year && <span className="date" style={{ color: "#2CDA00" }}> <span> $ </span> {work?.start?.year + "-" + work?.start?.month} </span>}
						{work?.end?.year && <span className="date" style={{ color: "#2CDA00" }}> <span> $ </span> {work?.end?.year + "-" + work?.end?.month} </span>}
						{work?.summary && <span className="summary"> <span> $ </span> {work?.summary} </span>}
						{work?.highlights && work?.highlights.map((item, index) => <span key={index}> {`->`} {item} </span>)}
					</div>
				</div> :
				curMode == "freelance" ?
				<div className="work-info">
					<div className="work-info-text" >
						{
							project?.link &&
							<span>
								<span>$ </span>
								<a
									href={project.link}
									style={{
										color: "inherit",
										cursor: "pointer"
									}}
								>
									{project.link}
								</a>
							</span>
						}

						{project?.company && <span className="company"> <span> $ </span> {project?.company} </span>}
						{project?.date && <span className="date" style={{ color: "#2CDA00" }}> <span> $ </span> {project?.date} </span>}
						{project?.summary && <span className="summary"> <span> $ </span> {project?.summary} </span>}
						{project?.highlights && project?.highlights.map((item, index) => <span key={index}> {`->`} {item} </span>)}
					</div>
				</div> :
				<div className="work-info">
					<div className="work-info-text">
						{achievement?.link &&
							<span>
								<span>$ </span>
								<a
									href={achievement.link}
									style={{
										color: "inherit",
										cursor: "pointer"
									}}
								>
									{achievement.link}
								</a>
							</span>
						}

						{
							achievement?.head &&
							<span>
								<span>$ </span>
								{achievement.head}
							</span>
						}

						{
							achievement?.desc &&
							<span>
								<span>$ </span>
								{achievement.desc}
							</span>
						}
					</div>
				</div>
			}






		</div>
	);
};

export default Work;