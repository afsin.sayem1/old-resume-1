const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlMinimizerPlugin = require('html-minimizer-webpack-plugin');

module.exports = {
	entry: {
		main: ['./src/index.js', './src/index.css']
	},

	mode: "production",

	output: {
		path: path.resolve(__dirname, 'dist/'),
		filename: '[name].js',
	},

	module: {
		rules: [
			{
				test: /\.css$/i,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.s[ac]ss$/i,
				use: ['style-loader', 'css-loader', 'sass-loader'],
			},
			{
				test: /\.(js|jsx)$/,
				include: path.resolve(__dirname, 'src/'),
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
						plugins: [
							[
								'@babel/plugin-transform-runtime',
								{
									regenerator: true,
								},
							],
						],
					},
				},
			},
		],
	},

	devServer: {
		port: '8888',
		static: {
			directory: path.resolve(__dirname, 'dist'),
		},
		open: true,
		hot: true,
		liveReload: true,
	},

	resolve: {
		/** "extensions"
		 * If multiple files share the same name but have different extensions, webpack will
		 * resolve the one with the extension listed first in the array and skip the rest.
		 * This is what enables users to leave off the extension when importing
		 */
		extensions: ['.js', '.jsx', '.json'],
	},

	plugins: [
		new CopyWebpackPlugin({
			patterns: [
				{
					from: path.resolve(__dirname, 'src/index.html'),
					to: path.resolve(__dirname, 'dist'),
				},
			],
		}),
		new webpack.DefinePlugin({
			'typeof CANVAS_RENDERER': JSON.stringify(true),
			'typeof WEBGL_RENDERER': JSON.stringify(true),
		}),
	],
	optimization: {
    minimize: true,
    minimizer: [
      // For webpack@5 you can use the `...` syntax to extend existing minimizers (i.e. `terser-webpack-plugin`), uncomment the next line
      // `...`
      new HtmlMinimizerPlugin({
				test: /\.html/i,
			}),
			new TerserPlugin({
        test: /\.js(\?.*)?$/i,
      }),
    ],
  },
};
